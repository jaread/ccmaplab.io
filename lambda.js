// This code is deployed on AWS Lambda so that the
// real CLIENT_SECRET is not revealed.
const https = require('https');

const CLIENT_ID = "39609";
const CLIENT_SECRET = "secret";
const ALLOWED_ORIGINS = ['http://localhost', 'https://ccmap.gitlab.io'];

exports.handler = async (event) => {
  const code = event.queryStringParameters.code;
  const grantType = event.queryStringParameters.grant_type;
  const refreshToken = event.queryStringParameters.refresh_token;
  const origin = event.headers.origin;
  if (!ALLOWED_ORIGINS.includes(origin)) {
    return { statusCode: 403 };
  }

  let path = `/oauth/token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=${grantType}`;
  if (code) {
    path += `&code=${code}`;
  }
  if (refreshToken) {
    path += `&refresh_token=${refreshToken}`;
  }
  const options = {
    hostname: 'www.strava.com',
    port: 443,
    path: path,
    method: 'POST',
    headers: {}
  };

  return new Promise((resolve, reject) => {
    const req = https.request(options, res => {
      res.on('data', d => {
        resolve({
          statusCode: res.statusCode,
          headers: {
            'Access-Control-Allow-Origin': origin,
          },
          body: JSON.stringify(JSON.parse(d)),
        });
      });
    });
    req.on('error', reject);
    req.end();
  });
};
